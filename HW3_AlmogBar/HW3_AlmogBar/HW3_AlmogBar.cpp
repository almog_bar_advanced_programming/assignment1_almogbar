// HW3_AlmogBar.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Vector.h"
#include <iostream>

int main()
{
	Vector* v = new Vector(5);
	v->push_back(1);
	v->push_back(2);
	v->push_back(3);
	v->push_back(4);
	v->push_back(5);
	v->push_back(6);
	std::cout << "size:" << v->size() << std::endl;
	std::cout << "resize factor:" << v->resizeFactor() << std::endl;
	std::cout << "empty:" << v->empty() << std::endl;
	v->reserve(10);
	std::cout << "capacity:" << v->capacity() << std::endl;
	v->resize(12);
	std::cout << "capacity:" << v->capacity() << std::endl;
	v->assign(1);
	v->resize(15, 2);
	Vector* v1 = new Vector(*v);
	v->push_back(100);
	v1 = v;
	std::cout << (*v1)[0] << std::endl;
	std::cout << (*v1)[100] << std::endl;
    return 0;
}

