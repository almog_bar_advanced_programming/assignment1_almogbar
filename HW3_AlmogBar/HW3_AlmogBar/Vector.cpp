#include "stdafx.h"
#include "Vector.h"
#include <iostream>

/*
constructor. creates new vector and initiates its values.
input: capacity of vector
output: none
*/
Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	_elements = new int[n];
	_capacity = n;
	_size = 0;
	_resizeFactor = n;
}

/*
destructor. frees all memory assigned to object
input: none
output: none
*/
Vector::~Vector()
{
	delete[] _elements;
}

/*
function returns size of vector
input: none
output: vector size
*/
int Vector::size() const 
{
	return _size;
}

/*
function returns capacity of vector
input: none
output: vector capacity
*/
int Vector::capacity() const 
{
	return _capacity;
}

/*
function returns vector's resize factor
input: none
output: vector resize factor
*/
int Vector::resizeFactor() const 
{
	return _resizeFactor;
}

/*
function checks if vector is empty
input: none
output: 0 if true, 1 if false
*/
bool Vector::empty() const 
{
	return _size == 0;
}

/*
function adds element at the end of the vector
input: value to add
output: none
*/
void Vector::push_back(const int& val) 
{
	if (_size == _capacity)
	{
		reserve(_capacity+_resizeFactor);
	}
	_elements[_size] = val;
	_size++;
}

/*
function removes and returns the last element of the vector
input: none
output: last element of vector
*/
int Vector::pop_back() 
{
	if (empty())
	{
		std::cout << "error: pop from empty vector" << std::endl;
		return -9999;
	}
	int ans = _elements[_size-1];
	_elements[_size] = 0; //??erase??
	_size--;
	return ans;
}

/*
function changes the capacity of the vector
input: new capacity
output: none
*/
void Vector::reserve(int n) 
{
	if (n>_capacity)
	{
		int mullNum = ((n - _capacity) / _resizeFactor) + 1;
		_capacity += _resizeFactor*mullNum;
		int* newElements = new int[_capacity];
		for (int i = 0; i < _size; i++)
		{
			newElements[i] = _elements[i];
		}
		delete[] _elements;
		_elements = newElements;
	}
}

/*
function changes size to n, unless n is greater than the vector's capacity
input: new size
output: none
*/
void Vector::resize(int n) 
{
	if (n<=_capacity)
	{
		_capacity = n;
	}
	else if (n>_capacity)
	{
		reserve(n);
	}
}

/*
function assigns a new value to all elements
input: value to be assigned
output: none
*/
void Vector::assign(int val) 
{
	for (int i = _size; i < _capacity; i++)
	{
		_elements[i] = val;
		_size++;
	}
}

/*
function changes size and adds a value to all empty elements
input: new size, value to be added
*/
void Vector::resize(int n, const int& val) 
{
	resize(n);
	assign(val);
}

/*
constructor. creates a new vector and initiates its values to a given vector's values
input: vector to copy its values
output: none
*/
Vector::Vector(const Vector& other)
{
	_capacity = other._capacity;
	_size = other._size;
	_resizeFactor = other._resizeFactor;
	_elements = new int[_capacity];
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i];
	}
}

/*
function realises = operator, which copies all values of a given vector
input: other vector
output: vector with copied values
*/
Vector& Vector::operator=(const Vector& other)
{
	delete[] _elements;
	_capacity = other._capacity;
	_size = other._size;
	_resizeFactor = other._resizeFactor;
	_elements = new int[_capacity];
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i];
	}
	return *this;
}

/*
function realises [] operator, which gets the value in a certain index of the elements
input: index
output: value in index
*/
int& Vector::operator[](int n) const
{
	if (n >= _size)
	{
		std::cout << "error: index out of range" << std::endl;
		return _elements[0];
	}
	return _elements[n];
}